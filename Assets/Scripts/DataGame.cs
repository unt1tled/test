﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataGame : MonoBehaviour
{
	[HideInInspector] public int CellsAmountX;
	[HideInInspector] public int CellsAmountY;
	[HideInInspector] public int MemoryTime;
	[HideInInspector] public int DragsCount;


}
