﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Board : MonoBehaviour
{
	private DataGame _dataGame;
	
	private Cell[,] _cellsBoard;
	private Cell[] _cellsDrag;
	
	private int[] _numbersDrag;
	private int[,] _numbersToCheck;
	
	[SerializeField] private GridLayoutGroup _gridBoard;
	[SerializeField] private GridLayoutGroup _gridDrag;
	
	[SerializeField] private GameObject _cellPrefab;

	

	private void Awake()
	{
		_dataGame = FindObjectOfType<DataGame>();

		EventManager.OnStarted += FillBoard;
		EventManager.OnGameplayed += FillDragPanel;
		EventManager.OnGameplayed += HideBoard;
		EventManager.OnChecked += Check;
	}

	

	void FillBoard()
	{
		_gridBoard.constraintCount = _dataGame.CellsAmountX;
		
		_cellsBoard = new Cell[_dataGame.CellsAmountX, _dataGame.CellsAmountY];
		_cellsDrag = new Cell[_dataGame.CellsAmountX*_dataGame.CellsAmountY];
		
		_numbersToCheck = new int[_dataGame.CellsAmountX,_dataGame.CellsAmountY];

		int[] allNumbers = Shuffler.Shuffle(Enumerable.Range(1, 99).ToArray());
		int[,] randomNumbers = new int[_dataGame.CellsAmountX, _dataGame.CellsAmountY];
		
		int index = 0;
		
		for (int i = 0; i < _dataGame.CellsAmountX; i++)
		{
			for (int j = 0; j < _dataGame.CellsAmountY; j++)
			{
				GameObject go = Instantiate(_cellPrefab);
				go.name = "Cell(" + i +"," + j + ")";
				go.tag = "BoardCell";
				go.transform.SetParent(_gridBoard.transform, false);
				
				Cell cell = go.GetComponent<Cell>();
				cell.Color = Random.ColorHSV();

				randomNumbers[i, j] = allNumbers[index];
				cell.Number = randomNumbers[i,j];


				_cellsBoard[i, j] = cell;
				_numbersToCheck[i, j] = cell.Number;
				
				_cellsDrag[index] = cell;
				
				index++;
			}
		}
	}

	void HideBoard()
	{
		for (int i = 0; i < _dataGame.CellsAmountX; i++)
		{
			for (int j = 0; j < _dataGame.CellsAmountY; j++)
			{
				_cellsBoard[i,j].Color = Color.gray;
				_cellsBoard[i,j].EnableText(false);
			}
		}
	}

	void FillDragPanel()
	{
		Cell[] cellsDragShuffle = Shuffler.Shuffle(_cellsDrag);
		for (int i = 0; i < cellsDragShuffle.Length; i++)
		{
			GameObject go = Instantiate(_cellPrefab);
			go.name = "Cell " + _cellsDrag[i].Number;
			go.transform.SetParent(_gridDrag.transform, false);
			go.AddComponent<DragHandler>();

			Cell cell = go.GetComponent<Cell>();

			cell.Color = _cellsDrag[i].Color;
			cell.Number = _cellsDrag[i].Number;
		}
	}

	void Check()
	{
		for (int i = 0; i < _dataGame.CellsAmountX; i++)
		{
			for (int j = 0; j < _dataGame.CellsAmountY; j++)
			{
				if (_cellsBoard[i, j].Number == _numbersToCheck[i, j])
				{
					_cellsBoard[i,j].Color = Color.green;
				}
				else
				{
					_cellsBoard[i,j].Color = Color.red;
				}
			}
		}
	}

	private void OnDestroy()
	{
		EventManager.OnStarted -= FillBoard;
		EventManager.OnGameplayed -= FillDragPanel;
		EventManager.OnGameplayed -= HideBoard;
		EventManager.OnChecked -= Check;
	}
}
