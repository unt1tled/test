﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour
{
	private Text _text;
	private Image _image;

	private int _number;
	public int Number
	{
		get { return _number; }
		set
		{
			_number = value;
			_text.text = _number.ToString();
		}
	}

	private Color _color;
	public Color Color
	{
		get { return _color; }
		set
		{
			_color = value;
			_image.color = _color;
		}
	}


	private void Awake()
	{
		_text = GetComponentInChildren<Text>();
		_image = GetComponent<Image>();
	}
	

	public void EnableText(bool value)
	{
		_text.gameObject.SetActive(value);
	}
	
	
}
