﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;


public static class Shuffler  
{

	public static T[] Shuffle<T>(T[] array)
	{
		for (int i = array.Length-1; i > 0; i--)
		{
			int rnd = Random.Range(0,i);
			T temp = array[i];
			
			array[i] = array[rnd];
			array[rnd] = temp;
		}
		return array;
	}
	
}
