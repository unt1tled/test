﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventManager 
{
	public delegate void StartAction();
	public static event StartAction OnStarted;
	
	public delegate void GameplayAction();
	public static event GameplayAction OnGameplayed;
	
	public delegate void CanCheckAction();
	public static event CanCheckAction OnCanChecked;
	
	public delegate void CheckAction();
	public static event CheckAction OnChecked;

	public static void OnStart()
	{
		if (OnStarted != null)
		{
			OnStarted();
		}
	}

	public static void OnGameplay()
	{
		if (OnGameplayed != null)
		{
			OnGameplayed();
		}
			
	}
	
	public static void OnCanCheck()
	{
		if (OnCanChecked != null)
		{
			OnCanChecked();
		}
	}

	public static void OnCheck()
	{
		if (OnChecked != null)
		{
			OnChecked();
		}
	}
	
}
