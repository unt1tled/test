﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
	private DataGame _dataGame;
	private UIController _uiController;
	private int _time;

	private void Awake()
	{
		_dataGame = FindObjectOfType<DataGame>();
		_uiController = FindObjectOfType<UIController>();

		EventManager.OnStarted += StartTimer;
	}

	IEnumerator Counter()
	{
		_uiController.TextTimer.text = _time.ToString();
		yield return new WaitForSeconds(1);
		_time--;

		if (_time > 0)
		{
			StartCoroutine(Counter());
		}
		else
		{
			EventManager.OnGameplay();
		}
	}

	void StartTimer()
	{
		_time = _dataGame.MemoryTime;
		
		StartCoroutine(Counter());
	}

	private void OnDestroy()
	{
		EventManager.OnStarted -= StartTimer;
	}
}
