﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Internal.Experimental.UIElements;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
	public GameObject PanelGame;
	public GameObject PanelSettings;
	public GameObject PanelBoard;
	public GameObject PanelDrag;

	public GameObject[] Panels;

	public Button ButtonStart;
	public Button ButtonCheck;
	public Button ButtonSettings;
	public Button ButtonBack;
	public Button ButtonRestart;

	public Slider SliderMemoryTime;
	public Slider SliderCellsAmountX;
	public Slider SliderCellsAmountY;

	public Text TextMemoryTimeValue;
	public Text TextCellsAmountXValue;
	public Text TextCellsAmountYValue;
	public Text TextTimer;

	private DataGame _dataGame;
	

	private void Awake()
	{
		_dataGame = FindObjectOfType<DataGame>();
		
		ButtonSettings.onClick.AddListener(OnSettings);
		ButtonBack.onClick.AddListener(OnBack);
		
		SliderMemoryTime.onValueChanged.AddListener(delegate {OnMemoryTimeChange(); });
		SliderCellsAmountX.onValueChanged.AddListener(delegate{OnCellsAmountXChange();});
		SliderCellsAmountY.onValueChanged.AddListener(delegate{OnCellsAmountYChange();});
		
		OnMemoryTimeChange();
		OnCellsAmountXChange();
		OnCellsAmountYChange();

		EventManager.OnStarted += OnStart;
		EventManager.OnGameplayed += OnGamePlay;
		EventManager.OnCanChecked += OnCanCheck;
		EventManager.OnChecked += OnCheck;
	}

	void OnStart()
	{
		ButtonStart.gameObject.SetActive(false);
		PanelBoard.SetActive(true);
		ButtonSettings.gameObject.SetActive(false);
		TextTimer.gameObject.SetActive(true);
	}

	void OnGamePlay()
	{
		PanelDrag.SetActive(true);
		TextTimer.gameObject.SetActive(false);
	}

	void OnCanCheck()
	{
		PanelDrag.SetActive(false);
		ButtonCheck.gameObject.SetActive(true);
	}

	public void OnCheck()
	{
		ButtonCheck.gameObject.SetActive(false);
		ButtonRestart.gameObject.SetActive(true);
	}

	void OnSettings()
	{
		ChangePanel(PanelSettings);
	}

	void OnBack()
	{
		ChangePanel(PanelGame);
	}

	public void ChangePanel(GameObject panel)
	{
		foreach (var obj in Panels)
		{
			obj.SetActive(false);
			panel.SetActive(true);
		}
	}

	public void Setup()
	{
		ChangePanel(PanelGame);
		
		PanelBoard.SetActive(false);
		PanelDrag.SetActive(false);
		
		ButtonStart.gameObject.SetActive(true);
		ButtonCheck.gameObject.SetActive(false);
		ButtonSettings.gameObject.SetActive(true);
		TextTimer.gameObject.SetActive(false);
	}

	private void OnDestroy()
	{
		ButtonSettings.onClick.RemoveAllListeners();
		ButtonSettings.onClick.RemoveAllListeners();
		
		SliderCellsAmountX.onValueChanged.RemoveAllListeners();
		SliderCellsAmountY.onValueChanged.RemoveAllListeners();
		SliderMemoryTime.onValueChanged.RemoveAllListeners();
		
		EventManager.OnStarted -= OnStart;
		EventManager.OnGameplayed -= OnGamePlay;
		EventManager.OnCanChecked -= OnCanCheck;
		EventManager.OnChecked -= OnCheck;
	}

	 void OnMemoryTimeChange()
	{
		TextMemoryTimeValue.text = SliderMemoryTime.value.ToString();
		_dataGame.MemoryTime = (int)SliderMemoryTime.value;
	}

	void OnCellsAmountXChange()
	{
		TextCellsAmountXValue.text = SliderCellsAmountX.value.ToString();
		_dataGame.CellsAmountX = (int)SliderCellsAmountX.value;
	}
	
	void OnCellsAmountYChange()
	{
		TextCellsAmountYValue.text = SliderCellsAmountY.value.ToString();
		_dataGame.CellsAmountY = (int) SliderCellsAmountY.value;
	}
}
