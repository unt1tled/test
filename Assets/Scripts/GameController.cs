﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
	private UIController _uiController;

	private void Awake()
	{
		_uiController = FindObjectOfType<UIController>();
	}

	private void Start()
	{
		_uiController.Setup();
	}

	public void OnStart()
	{
		EventManager.OnStart();
	}

	public void OnCheck()
	{
		EventManager.OnCheck();
	}

	public void OnRestart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
