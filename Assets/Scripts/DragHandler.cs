﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
	private Vector2 _originPos;
	private DataCached _dataCached;
	private DataGame _dataGame;

	private GameObject _startDragGO;
	private GameObject _endDragGO;

	private void Awake()
	{
		_dataCached = FindObjectOfType<DataCached>();
		_dataGame = FindObjectOfType<DataGame>();
	}


	public void OnBeginDrag(PointerEventData eventData)
	{
		_originPos = transform.position;
		_startDragGO = gameObject;
	}
	
	public void OnDrag(PointerEventData eventData)
	{
		Vector2 pos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(_dataCached.Canvas.transform as RectTransform, Input.mousePosition, _dataCached.Canvas.worldCamera, out pos);
		transform.position = _dataCached.Canvas.transform.TransformPoint(pos);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		List<RaycastResult> results = new List<RaycastResult>();
		_dataCached.GraphicRaycaster.Raycast(eventData,results);

		foreach (var obj in results)
		{
			if (obj.gameObject.CompareTag("BoardCell"))
			{
				_endDragGO = obj.gameObject;
				_endDragGO.tag = "Draged";
				

				Cell startDragCell = _startDragGO.GetComponent<Cell>();
				Cell endDragCell = _endDragGO.GetComponent<Cell>(); 
				
				endDragCell.Number = startDragCell.Number;
				endDragCell.Color = startDragCell.Color;
				endDragCell.EnableText(true);
				
				_startDragGO.SetActive(false);

				_dataGame.DragsCount++;

				if (_dataGame.DragsCount == _dataGame.CellsAmountX * _dataGame.CellsAmountY)
				{
					EventManager.OnCanCheck();
				}
			}
			else
			{
				transform.position = _originPos;
			}
		}
	}

	
}
